---
layout: post
title:  "Publicando uma aplicação AspNet Core no Heroku"
date:   2017-05-31 23:49:00 -0200
categories: aspnet heroku docker dotnet
author: 'Alan Lira'
author_url: 'http://github.com/lira92'
author_url_label: 'Alan Lira'
author_url_avatar: 'https://avatars3.githubusercontent.com/u/10679262'
disqus_identifier: a815331eadf93f8f
comments: true
highlights: false
---

Olá Pessoal, o objetivo deste post é mostrar como você pode publicar uma aplicação AspNet Core no heroku. Mas o que é esse Asp Net Core? O Asp Net por muito tempo só era suportado em ambientes Windows, onde a plataforma .net era suportada, mas a algum tempo a Microsoft lançou uma versão reescrita do Asp Net e do .Net, que deu o nome de Core, pois tem somente as funcionalidades principais que foram portadas para o .Net Core e tem suporte a multiplataforma, além de inúmeras melhorias de desempenho. Bom, e o Heroku? O heroku é uma das plataformas Paas (Platforma as a Service) mais conhecidas e que atualmente suporta diversas linguagens para aplicações web, porém o Heroku ainda não suporta AspNet Core nativamente, existem alguns buildpacks (Mecanismo de deploy que prepara o ambiente para suportar a aplicação) não oficiais, mas como eu tentei utilizar e não consegui, vou utilizar o docker e o suporte a ele do heroku para fazer o deploy da aplicação.

## Pré requisitos ##
1. Windows, Mac ou Linux
2. [dotnet sdk](https://www.microsoft.com/net/core)
3. [docker](https://docs.docker.com/engine/installation)
4. [heroku toolbet](https://devcenter.heroku.com/articles/heroku-cli)

## Criando a aplicação ##
Crie uma pasta com o nome do projeto, no meu caso vou dar o nome de AspNetOnHeroku. Logo em seguida, na pasta do projeto, com o dotnet instalado execute o comando new para criar o projeto:

    dotnet new mvc --auth None --framework netcoreapp1.1

O comando new da ferramenta de linha de comando do .net core, automatiza a criação de projetos com templates prédefinidos, neste caso iremos criar uma aplicação web ASP NET MVC, sem autenticação e com a versão do framework 1.1. Se tudo ocorrer conforme previsto, você deverá ver algo como:

    The template "ASP.NET Core Web App" created successfully.

Os arquivos da nossa aplicação já estão na pasta, porém, diversas depêndencias (.dll) são necessárias para que possamos compilar nosso projeto, essas depêndencias são gerenciadas pelo gerenciador de pacotes *Nuget* para restaurar o pacotes do *Nuget*, execute:

    dotnet restore
    
Com as dependências resolvidas, iremos exportar os arquivos arquivos para deploy, usando o comando publish e como parametro passamos a configuração de build "Release", ideal para ambiente de produção:

    dotnet publish -c Release
 
## Configurando o Dockerfile ##

Navegue até a pasta bin/Release/netcoreapp1.1/publish e crie um arquivo chamado Dockerfile, sem extensão alguma e com o conteúdo abaixo:

    FROM microsoft/dotnet:1.1.2-runtime

	WORKDIR /app
	COPY . .

	CMD ASPNETCORE_URLS=http://*:$PORT dotnet AspNetOnHeroku.dll

Neste arquivo estamos configurando para usar a imagem oficial para linux da microsoft pro .net core, e configurando o host e a porta para o servidor da aplicação, onde está especificado AspNetOnHeroku.dll altere para o nome do seu projeto.

## Instalando Registry do Heroku ##

Agora vamos precisar instalar o registry do heroku, que é um plugin que possibilita trabalhar com containers, é ele que vai se responsabilizar em entregar nosso container da aplicação no ambiente do heroku, para isso com o heroku toolbet instalado, execute:

    heroku plugins:install heroku-container-registry
    
Depois de instalado, iremos executar o login no container registry:

    heroku container:login

## Executando o Docker ##

Com o docker instalado, e estando na raiz do projeto execute:

    docker build -t aspnet-on-heroku ./bin/Release/netcoreapp1.1/publish

O docker build irá ler o nosso Dockerfile e gerar uma imagem a partir dele com o nome de aspnet-on-heroku.
Agora para que a nossa imagem local possa ser enviada a nossa imagem remota, no heroku, execute:

    docker tag aspnet-on-heroku registry.heroku.com/aspnet-on-heroku/web

aspnet-on-heroku é o nome do meu app que cadastrei através do painel do heroku, mas se você ainda não possui um app criado, pode fazer isso executando "heroku create".

## Fazendo o deploy ##

Para fazer o deploy, enviando a imagem local para o container registry do heroku, execute:

    docker push registry.heroku.com/aspnet-on-heroku/web
    
Isso irá demorar um pouco dependendo da sua conexão com a internet, depois de concluído vamos navegar até o endereço: [https://aspnet-on-heroku.herokuapp.com/](https://aspnet-on-heroku.herokuapp.com/). Se você ver algo como:

![enter image description here](http://pix.toile-libre.org/upload/original/1496109184.png)

Parabéns! Você publicou sua primeira aplicação AspNet Core no Heroku.
Caso você tenha algum erro, consulte os logs do heroku acessando o menu abaixo:

![enter image description here](http://pix.toile-libre.org/upload/original/1496109277.png)

## Conclusão ##

O Asp net e o .Net passaram por uma reinvenção, e tiveram muitas melhorias, e a maior delas é a que juntos podem ser executados em diferentes plataformas, facilitando o uso de recursos como docker para entregar sua aplicação em containers e o heroku como plataforma Paas que é muito conhecido pela facilidade e baixo custo para disponibilizar seu software.

*Mais informações sobre docker?* [https://leanpub.com/dockerparadesenvolvedores](https://leanpub.com/dockerparadesenvolvedores)

*Fonte:* http://dotnetthoughts.net/hosting-aspnet-core-applications-on-heroku-using-docker/